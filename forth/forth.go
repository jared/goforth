package forth

type Forth struct {
	data Stack
	ret  Stack
	dict Dict
	tib  Cell
	in   Cell
	code []Cell
	pc   Cell
}

func NewForth() (f *Forth) {
	f = &Forth{}
	f.CoreWords()
	f.pc = 0
	f.tib = []byte{}
	return
}

func (f *Forth) SetTib(s string) {
	f.tib = []byte(s)
}

func (f *Forth) TIB() {
	f.data.Push(f.tib)
}

func (f *Forth) TEST1() {
	println("test1")
}

func (f *Forth) TEST2() {
	println("test2")
}

func (f *Forth) PUSHPC() {
	val := f.ReadIncPC()
	f.data.Push(val)
}

func (f *Forth) Code(c Cell) {
	f.code = append(f.code, c)
}

func (f *Forth) Execute() {
	word := f.ReadIncPC()
	word.(func())()
}

func (f *Forth) ReadIncPC() Cell {
	word := f.ReadPC()
	f.IncPC()
	return word
}

func (f *Forth) IncPC() {
	i := f.pc.(int)
	i++
	f.pc = Cell(i)
}

func (f *Forth) ReadPC() Cell {
	return f.code[f.pc.(int)]
}
