package forth

type Word struct {
	Label string
	Code  func()
}

type Dict struct {
	Words []*Word
}

func (d *Dict) AddWord(label string, code func()) {
	d.Words = append(d.Words, NewWord(label, code))
}

func NewWord(label string, code func()) *Word {
	return &Word{
		Label: label,
		Code:  code,
	}
}

func (f *Forth) CoreWords() {
	f.dict.AddWord("QUIT", f.QUIT)
}

func (f *Forth) QUIT() {
	f.data.Empty()
	f.ret.Empty()
}
