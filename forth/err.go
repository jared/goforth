package forth

type ErrorType Cell

const (
	StackUnderflow = iota
)

type Error struct {
	error
	Type ErrorType
}

func (e *Error) Error() (s string) {
	s = "Unknown"
	switch e.Type {
	case StackUnderflow:
		s = "Stack Underflow"
	}
	return
}

func NewError(errorType ErrorType) *Error {
	return &Error{Type: errorType}
}

func Underflow() *Error {
	return NewError(StackUnderflow)
}
