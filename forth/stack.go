package forth

type Stack struct {
	data []Cell
}

func (s *Stack) Push(el Cell) {
	s.data = append(s.data, el)
}

func (s *Stack) Pop() (head Cell, e error) {
	l := len(s.data)
	if l == 0 {
		return nil, Underflow()
	}
	head, s.data = s.data[l-1], s.data[:l-1]
	return
}

func (s *Stack) Empty() {
	s.data = nil
}
