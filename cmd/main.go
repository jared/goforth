package main

import (
	"fmt"
	"goforth/forth"
)

func main() {
	f := &forth.Forth{}
	f.Initialize()

	fi := f.Initialize
	println(fmt.Sprintf("%p", fi))
}
