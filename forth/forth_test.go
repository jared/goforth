package forth

import (
	"fmt"
	"testing"
)

func Test_forth(t *testing.T) {
	f := NewForth()

	f.Code(f.PUSHPC)
	f.Code(2)
	f.Execute()
	v, _ := f.data.Pop()
	println(fmt.Sprintf("%d", v))
	f.SetTib("  WORD")

	f.TIB()
	tib, _ := f.data.Pop()
	println(string(tib.([]byte)))

}
